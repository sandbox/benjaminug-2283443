; Drupal.org release file.
core = 7.x
api = 2

; Basic contributed modules.

projects[ctools][version] = 1.6
projects[ctools][type] = "module"
projects[ctools][subdir] = "contrib"

projects[admin_menu][version] = 3.0-rc5
projects[admin_menu][type] = "module"
projects[admin_menu][subdir] = "contrib"

projects[context][version] = 3.6
projects[context][type] = "module"
projects[context][subdir] = "contrib"

projects[features][version] = 2.3
projects[features][type] = "module"
projects[features][subdir] = "contrib"

projects[strongarm][version] = 2.0
projects[strongarm][type] = "module"
projects[strongarm][subdir] = "contrib"

projects[views][version] = 3.8
projects[views][type] = "module"
projects[views][subdir] = "contrib"

projects[webform][version] = 3.21
projects[webform][type] = "module"
projects[webform][subdir] = "contrib"

projects[module_filter][version] = 2.0-alpha2
projects[module_filter][type] = "module"
projects[module_filter][subdir] = "contrib"

projects[entity][version] = 1.5
projects[entity][type] = "module"
projects[entity][subdir] = "contrib"

projects[jquery_update][version] = 2.5
projects[jquery_update][type] = "module"
projects[jquery_update][subdir] = "contrib"

projects[libraries][version] = 3.x-dev
projects[libraries][type] = "module"
projects[libraries][subdir] = "contrib"

projects[book_helper][version] = 1.0-beta3
projects[book_helper][type] = "module"
projects[book_helper][subdir] = "contrib"

projects[delta][version] = 3.0-beta11
projects[delta][type] = "module"
projects[delta][subdir] = "contrib"

projects[omega_tools][version] = 3.0-rc4
projects[omega_tools][type] = "module"
projects[omega_tools][subdir] = "contrib"

projects[colorbox][version] = 2.8
projects[colorbox][type] = "module"
projects[colorbox][subdir] = "contrib"

projects[colorbox_node][version] = 3.4
projects[colorbox_node][type] = "module"
projects[colorbox_node][subdir] = "contrib"
projects[colorbox_node][patches][] = "https://www.drupal.org/files/issues/colorbox_node-movefindHreffunction-2218251-8.patch"

projects[weight][version] = 2.4
projects[weight][type] = "module"
projects[weight][subdir] = "contrib"

projects[file_entity][version] = 2.0-beta1
projects[file_entity][type] = "module"
projects[file_entity][subdir] = "contrib"

projects[nice_menus][version] = 2.5
projects[nice_menus][type] = "module"
projects[nice_menus][subdir] = "contrib"

projects[pane][type] = "module"
projects[pane][download][type] = "git"
projects[pane][download][url] = "git://github.com/benjaminug/pane.git"
projects[pane][directory_name] = "pane"
projects[pane][subdir] = "contrib"

projects[views_flipped_table][version] = 1.0
projects[views_flipped_table][type] = "module"
projects[views_flipped_table][subdir] = "contrib"

projects[globalredirect][version] = 1.5
projects[globalredirect][type] = "module"
projects[globalredirect][subdir] = "contrib"

projects[transliteration][version] = 3.2
projects[transliteration][type] = "module"
projects[transliteration][subdir] = "contrib"

projects[webform2vtiger][version] = 1.0
projects[webform2vtiger][type] = "module"
projects[webform2vtiger][subdir] = "contrib"

projects[piwik][version] = 2.5
projects[piwik][type] = "module"
projects[piwik][subdir] = "contrib"

projects[pathauto][version] = 1.2
projects[pathauto][type] = "module"
projects[pathauto][subdir] = "contrib"

projects[css_browser_selector][version] = 1.1
projects[css_browser_selector][type] = "module"
projects[css_browser_selector][subdir] = "contrib"

projects[publishcontent][version] = 1.3
projects[publishcontent][type] = "module"
projects[publishcontent][subdir] = "contrib"

projects[advanced_help][version] = 1.1
projects[advanced_help][type] = "module"
projects[advanced_help][subdir] = "contrib"

projects[search_config][version] = 1.1
projects[search_config][type] = "module"
projects[search_config][subdir] = "contrib"

projects[xmlsitemap][version] = 2.0
projects[xmlsitemap][type] = "module"
projects[xmlsitemap][subdir] = "contrib"

projects[metatag][version] = 1.0-rc1
projects[metatag][type] = "module"
projects[metatag][subdir] = "contrib"

projects[token][version] = 1.5
projects[token][type] = "module"
projects[token][subdir] = "contrib"

projects[simple_subscription][version] = 1.0
projects[simple_subscription][type] = "module"
projects[simple_subscription][subdir] = "contrib"

projects[diff][version] = 3.2
projects[diff][type] = "module"
projects[diff][subdir] = "contrib"

projects[chosen][version] = 2.0-beta4
projects[chosen][type] = "module"
projects[chosen][subdir] = "contrib"

projects[schemaorg][version] = 1.0-beta4
projects[schemaorg][type] = "module"
projects[schemaorg][subdir] = "contrib"

projects[better_messages][version] = 1.x-dev
projects[better_messages][type] = "module"
projects[better_messages][subdir] = "contrib"

projects[devel][version] = 1.5
projects[devel][type] = "module"
projects[devel][subdir] = "contrib"

projects[faq][version] = 1.0-rc2
projects[faq][type] = "module"
projects[faq][subdir] = "contrib"

projects[simplenews][version] = 1.1
projects[simplenews][type] = "module"
projects[simplenews][subdir] = "contrib"

projects[appserver][version] = 1.0-beta3
projects[appserver][type] = "module"
projects[appserver][subdir] = "contrib"

projects[glossify][version] = 4.0-beta1
projects[glossify][type] = "module"
projects[glossify][subdir] = "contrib"

projects[field_views][version] = 1.0-alpha2
projects[field_views][type] = "module"
projects[field_views][subdir] = "contrib"

projects[votingapi][version] = 2.12
projects[votingapi][type] = "module"
projects[votingapi][subdir] = "contrib"

projects[references][version] = 2.1
projects[references][type] = "module"
projects[references][subdir] = "contrib"

projects[taxonomy_view_mode][version] = 1.0-alpha1
projects[taxonomy_view_mode][type] = "module"
projects[taxonomy_view_mode][subdir] = "contrib"

projects[adminimal_admin_menu][version] = 1.5
projects[adminimal_admin_menu][type] = "module"
projects[adminimal_admin_menu][subdir] = "contrib"

projects[nagios][version] = 1.2
projects[nagios][type] = "module"
projects[nagios][subdir] = "contrib"

includes[mountbatten_web_analytics] = https://raw.githubusercontent.com/mountbatten/mountbatten_makefiles/master/mountbatten_web_analytics.make
includes[mountbatten_seo] = https://raw.githubusercontent.com/mountbatten/mountbatten_makefiles/master/mountbatten_seo.make
includes[mountbatten_spam_protection] = https://raw.githubusercontent.com/mountbatten/mountbatten_makefiles/master/mountbatten_spam_protection.make
includes[mountbatten_web_media] = https://raw.githubusercontent.com/mountbatten/mountbatten_makefiles/master/mountbatten_web_media.make

; Base theme.
projects[omega][version] = 3.1
projects[omega][type] = "theme"

;Main theme
projects[devtrac7_theme][version] = "1.x-dev"
projects[devtrac7_theme][type] = "theme"

projects[adminimal_theme][version] = "1.18"
projects[adminimal_theme][type] = "theme"

libraries[colorbox][download][type] = "file"
libraries[colorbox][download][url] = "https://github.com/jackmoore/colorbox/archive/1.x.zip"
libraries[colorbox][directory_name] = "colorbox"
libraries[colorbox][destination] = "libraries"

libraries[chosen][download][type] = "file"
libraries[chosen][download][url] = "https://github.com/harvesthq/chosen/releases/download/v1.1.0/chosen_v1.1.0.zip"
libraries[chosen][directory_name] = "chosen"
libraries[chosen][destination] = "libraries"
